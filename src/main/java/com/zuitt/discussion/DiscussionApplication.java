package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
// Will require all routes within the "Discussion Application" to use "/greeting" as part of its routes
 @RequestMapping("/greeting")
//The "@RestController annotation" tells Spring Boot that this application will function as an endpoint that will be used in handling web requests
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	/*@GetMapping("/hello")
	//Maps a get request to the route "/hello" and the method "hello()"
	public String hello() {
		return "Hello World!";
	}

	// Routes with a string query
	// Dynamic data is obtained from the URL's string query
	// "%s" specifies that the value to be included in the format is of any data type
	// http://localhost:8080/hi?name=Joe

	@GetMapping("/hi")
	//Maps a get request to the route "/hello" and the method "hello()"
	public String hi(@RequestParam(value = "name", defaultValue = "John") String name) {
		return String.format("Hi %s", name);
	}

	// Multiple Parameters
	// http://localhost:8080/friend?name=Ash&friend=Pikachu
	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend){
		return String.format("Hello %s! My name is %s.", friend, name);
	}

	// Routes with path variables
	@GetMapping("/hello/{name}")
	// http://localhost:8080/hello/joe
	// @PathVariable annotation allows us to extract data directly from the URL
	public String courses (@PathVariable ("name") String name){
		return String.format("Nice to meet you %s!", name);
	}*/

	// ACTIVITY
	private ArrayList<String> enrollees = new ArrayList<String>(); // 1

	// http://localhost:8080/greeting/enroll?user=Jerome
	@GetMapping("/enroll") // 2
	public String enroll(@RequestParam(value = "user", defaultValue = "Cara") String user) {
		enrollees.add(user);
		return "Thank you for enrolling, " + user + "!";
	}

	// http://localhost:8080/greeting/getEnrollees
	@GetMapping("/getEnrollees") // 3
	public String getEnrollees() {
		return enrollees.toString();
	}

	// http://localhost:8080/greeting/nameage?name=Jill&age=31
	@GetMapping("/nameage") // 4
	public String nameAge(@RequestParam(value = "name", defaultValue = "Cara") String name, @RequestParam(value = "age", defaultValue = "18") String age) {
		return String.format("Hello %s! My age is %s.", name, age);
	}

	// http://localhost:8080/greeting/courses/java101
	@GetMapping("/courses/{id}") // 5
	public String courses(@PathVariable ("id") String id){
		if(id.equals("java101")) {
			return "Name: Java 101, Schedule: MWF 8:00 AM - 11:00 AM, Price: PHP 3000";
		}else if (id.equals("sql101")) {
			return "Name: SQL 101, Schedule: MWF 1:00 PM - 3:00 PM, Price: PHP 4000";
		}else if (id.equals("javaee101")) {
			return "Name: Java EE 101, Schedule: MWF 4:00 PM - 6:00 PM, Price: PHP 5000";
		}else {
			return "Course cannot be found";
		}
	}

}
